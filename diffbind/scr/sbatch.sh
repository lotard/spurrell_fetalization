#!/usr/bin/env bash
sbatch --export=ALL --job-name=$JOBNAME --error=logs/${JOBNAME}.err --output=logs/${JOBNAME}.out scr/run.sh
