module add parallel/20200222

# download gencode V24, munge
echo 'filtering gencode'
zcat downloaded/gencode.v24.annotation.gtf.gz | awk '{FS="\t";OFS="\t"} \
	$3=="transcript"&&$7=="+"&&($9~/protein_coding/||$9~/lincRNA/){print $1, $4-1000, $4+1000, $9} \
	$3=="transcript"&&$7=="-"&&($9~/protein_coding/||$9~/lincRNA/){print $1, $5-1000, $5+1000, $9} \
	' > tmp.tss.bed
mkdir -p tmp
mkdir -p peaks

# filter/clean-up peak files
echo 'filtering peaks'
parallel ./scr/peak.filter.helper.sh ::: `find . -name '*final.replicated.broadPeak.gz'`

rm tmp.tss.bed
