#!/bin/bash
#SBATCH --mem=120GB
#SBATCH --time=2:00:00
#SBATCH --cpus-per-task=10


date
if [ -z ${OUTDIR+x} ] || [ -z ${CONTRAST+x} ]
then
  echo "No OUTDIR or CONTRAST in env!"
else
  Rscript scr/metadataUpdate.R ${OUTDIR}/dbMetadata.txt && 
  Rscript scr/db.R $OUTDIR $CONTRAST
fi
date
