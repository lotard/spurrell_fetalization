reportF <- function(dbaObj, contrast, method, fold_t, fdr_t, outPath) {
    dbaObj <- dba.report(dbaObj, contrast=contrast, method=method)
    out <- as.data.frame(cbind(as.character(seqnames(dbaObj)), start(dbaObj), end(dbaObj), round(elementMetadata(dbaObj)[,2], 2), round(elementMetadata(dbaObj)[,3], 2), round(dbaObj$Fold, 2), dbaObj$"p-value", dbaObj$FDR))
    col1 <- gsub("Conc", "log2", colnames(elementMetadata(dbaObj))[2])
    col2 <- gsub("Conc", "log2", colnames(elementMetadata(dbaObj))[3])
    colnames(out) <- c("chrom", "start", "end", col1, col2, "log2_FC", "p", "FDR")
    out <- out[abs(as.numeric(as.character(out$log2_FC))) >= fold_t & as.numeric(as.character(out$FDR)) <= fdr_t,]
    write.table(out, file=outPath, col.names=T, row.names=F, sep="\t", quote=F)
    #return(out)
}

