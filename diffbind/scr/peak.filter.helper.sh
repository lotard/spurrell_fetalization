# remove non-standard chromsomes
# filter peaks for p>0.05 (in -log10 space)
# remove TSS-proximal peaks

echo $1
MOD=`echo $1 | sed -E 's:.*dnanexus/(.*)/ChIP-seq.*:\1:'`
zcat $1 | awk '$1 !~ /random$/' | awk '$9>=1.30103' | grep -v '^chr[XMYU]' > tmp/${MOD}.broadPeak
bedtools intersect -a tmp/${MOD}.broadPeak -b tmp.tss.bed -v  > peaks/${MOD}.distal.broadPeak
rm tmp/${MOD}.broadPeak

