#!/usr/bin/env bash
mkdir -p logs
JOBNAME=cai_homer
sbatch --export=ALL --job-name=$JOBNAME --error=logs/${JOBNAME}.err --output=logs/${JOBNAME}.out scr/run.sh
