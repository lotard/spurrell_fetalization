#!/bin/bash
#SBATCH --mem=5GB
#SBATCH --time=6:00:00
#SBATCH --partition lr5
#SBATCH -A pc_enhancer
#SBATCH -q lr_normal
#SBATCH --cpus-per-task=16


date
## raw background (all peaks present in at least two individuals)
findMotifsGenome.pl input/dcm_only.bed hg38 results/DCM_all_unfilt/ -size -500,500 -p 16 -nomotif -len 6,7,8,9,10,12,14 -bg input/all.bed
findMotifsGenome.pl input/fet_only.bed hg38 results/FET_all_unfilt/ -size -500,500 -p 16 -nomotif -len 6,7,8,9,10,12,14 -bg input/all.bed
findMotifsGenome.pl input/fet_dcm.bed hg38 results/FETDCM_all_unfilt/ -size -500,500 -p 16 -nomotif -len 6,7,8,9,10,12,14 -bg input/all.bed
date
