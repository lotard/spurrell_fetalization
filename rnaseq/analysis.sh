#!/bin/bash


mkdir htseq_counts
while read line;
do
    id=$(echo "$line" | awk '{FS="\t";OFS="\t"}{print $2}')
    fname=$(echo "$line" | awk -F"\t" '{FS="\t";OFS="\t"}{split($9, a, "/"); print a[9]}' | sed 's/.fastq.gz//g' | sed 's/.fq.gz//g')
    echo '#!/bin/sh' > htseq_"$id".sh
    echo "samtools_0.1.18 view -q1 -h "$id"/RNA-seq/"$fname"_star_genome.bam > "$id".temp.SAM" >> htseq_"$id".sh
    echo "htseq-count -s no "$id".temp.SAM gencode.v24.annotation.gtf.gz > htseq_counts/"$id".counts.txt" >> htseq_"$id".sh
    echo "rm "$id".temp.SAM" >> htseq_"$id".sh
    sbatch --export=ALL htseq_"$id".sh
done < <(grep -v include input/Metadata_cs_heart_20181211.rna.txt)


#edgeR
R --vanilla < dataPrep.R
R --vanilla < edgeR.R