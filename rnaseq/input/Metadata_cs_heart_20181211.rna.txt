include	dataset	experiment	biosample	species	paired	stranded	read	filename
TRUE	11963_CS22	11963_RNAseq	11963_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_3B_cs22_11963_heart_RNA.fastq.gz
TRUE	12434_PCW10	12434_RNAseq	12434_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_3C_pcw10_12434_RNA.fastq.gz
TRUE	12522_PCW10	12522_RNAseq	12522_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_3D_pcw10_12522_RNA.fastq.gz
TRUE	12569_PCW10	12569_RNAseq	12569_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPSA_060916_2H_10pcw12569_RNA.fastq.gz
TRUE	12570_LV_PCW17	12570_RNAseq	12570_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_4B_pcw17_12570_lv_RNA.fastq.gz
TRUE	LV1028	LV1028_RNAseq	LV1028_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_060315_2A_LV1028_RNA.fastq.gz
TRUE	LV1033	LV1033_RNAseq	LV1033_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_060315_2B_LV1033_RNA.fastq.gz
TRUE	LV1072	LV1072_RNAseq	LV1072_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_1A_LV1072_RNA.fastq.gz
TRUE	LV1121	LV1121_RNAseq	LV1121_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_081216_1D_LV1121_RNA.fastq.gz
TRUE	LV1160	LV1160_RNAseq	LV1160_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_060315_2D_LV1160_RNA.fastq.gz
TRUE	LV1183	LV1183_RNAseq	LV1183_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_062215_1B_LV1183_RNA.fastq.gz
TRUE	LV1200	LV1200_RNAseq	LV1200_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_042916_1B_LV1200_RNA.fastq.gz
TRUE	LV1202	LV1202_RNAseq	LV1202_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_1B_LV1202_RNA.fastq.gz
TRUE	LV1215	LV1215_RNAseq	LV1215_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_042916_1D_LV1215_RNA.fastq.gz
TRUE	LV1223	LV1223_RNAseq	LV1223_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_081216_1E_LV1223_RNA.fastq.gz
TRUE	LV1226	LV1226_RNAseq	LV1226_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPSA_062714_036_Adult_LV_Normal_1226.fq.gz
TRUE	LV1248	LV1248_RNAseq	LV1248_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_102414_005A_LV1248_totrna.fastq.gz
TRUE	LV1259	LV1259_RNAseq	LV1259_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPSA_062714_037_Adult_LV_Normal_1259.fq.gz
TRUE	LV1264	LV1264_RNAseq	LV1264_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_073114_001A_LV1264_totrna.fastq.gz
TRUE	LV1273	LV1273_RNAseq	LV1273_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_042916_1F_LV1273_RNA.fastq.gz
TRUE	LV1275	LV1275_RNAseq	LV1275_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_1D_LV1275_RNA.fastq.gz
TRUE	LV1277	LV1277_RNAseq	LV1277_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_2A_LV1277_RNA.fastq.gz
TRUE	LV1290	LV1290_RNAseq	LV1290_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_2B_LV1290_RNA.fastq.gz
TRUE	LV1294	LV1294_RNAseq	LV1294_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_042916_1G_LV1294_RNA.fastq.gz
TRUE	LV1300	LV1300_RNAseq	LV1300_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_060315_2E_LV1300_RNA.fastq.gz
TRUE	LV1304	LV1304_RNAseq	LV1304_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_060315_2F_LV1304_RNA.fastq.gz
TRUE	LV1325	LV1325_RNAseq	LV1325_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_102414_005B_LV1325_totrna.fastq.gz
TRUE	LV1342	LV1342_RNAseq	LV1342_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_073114_001B_LV1342_totrna.fastq.gz
TRUE	LV1356	LV1356_RNAseq	LV1356_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_2C_LV1356_RNA.fastq.gz
TRUE	LV1371	LV1371_RNAseq	LV1371_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_121215_2D_LV1371_RNA.fastq.gz
TRUE	LV1400	LV1400_RNAseq	LV1400_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_042916_2B_LV1400_RNA.fastq.gz
TRUE	LV1433	LV1433_RNAseq	LV1433_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPSA_060916_2E_LV1433_RNA.fastq.gz
TRUE	LV1437	LV1437_RNAseq	LV1437_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_030216_2G_LV1437_rna.fastq.gz
TRUE	LV1452	LV1452_RNAseq	LV1452_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_042916_2C_LV1452_RNA.fastq.gz
TRUE	LV1471	LV1471_RNAseq	LV1471_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_042916_2D_LV1471_RNA.fastq.gz
TRUE	LV1532	LV1532_RNAseq	LV1532_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_042916_2E_LV1532_RNA.fastq.gz
TRUE	LV1535	LV1535_RNAseq	LV1535_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPCS_081216_1F_LV1535_RNA_REDO.fastq.gz
TRUE	LV1558	LV1558_RNAseq	LV1558_RNA	Homo sapiens	Single-end	Forward	1	/global/dna/projectdirs/RD/heart/cailyn_paper_heart_samples_fastq/rnaseq/LPSA_060916_2F_LV1558_RNA.fastq.gz
