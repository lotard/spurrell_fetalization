
plotLoads <- function(C1, C2, xlab, ylab, cols, textFlag) {
    xlim <- round(range(C1) * 1.3, 1)
    ylim <- round(range(C2) * 1.3, 1)
    if (textFlag) {
        cex=0.6
    } else {
        cex=1.5
    }
    plot(C2~C1, col=cols, xlim=xlim, ylim=ylim, xlab=xlab, ylab=ylab, cex=cex, pch=20)
    if (textFlag) {
        text(names(C1), x=C1, y=C2+0.02, col=cols, cex=0.75)
    }
}

#
bplot <- function(scaled_pca, pc, main, feat, col, ylim, test) {
    if (test) {
        p <- wilcox.test(scaled_pca$x[,pc]~feat)$p.value
        main <- paste(main, " (p = ", formatC(p, 2), ")", sep="")
    }
    boxplot(scaled_pca$x[,pc]~feat, main=main, col=unique(col), horizontal=T, las=1, ylim=ylim, border="black", outline=F, ylab="")
    val <- jitter(as.numeric(as.factor(feat)), 0.5)
    points(scaled_pca$x[,pc], val, col="grey", pch=16)
    points(scaled_pca$x[,pc], val, col="black", pch=1)
}

runPCA <- function(data, group_label, outFolder, suffix, cols, cols_2, topN, nComp, test) {
    #
    #exclude low expression - pick top in terms of sd
    sd <- apply(data, 1, sd)
    data <- data[order(sd, decreasing=T),][1:topN,]
    data_scaled <- t(apply(data, 1, scale))
    colnames(data_scaled) <- colnames(data)
    #
    scaled_pca <- prcomp(t(data_scaled))
    vars <- scaled_pca$sdev^2
    vars <- vars/sum(vars)
    #
    axisLbls <- c()
    for (i in 1:nComp) {
        axisLbls <- c(axisLbls, paste("PC", i, " (", round(vars[i]*100,1), "%)", sep=""))
    }
    #
    pdf(paste(outFolder, "/PCA.", suffix, ".top", topN, ".variance.pdf", sep=""), width=5, height=4)
    par(mar=c(6,7,2,2))
    plot(vars, type="h", lwd="5", ylab="Fraction (0-1) of variance", xlab="PC", las=1, ylim=c(0,0.3))
    dev.off()
    #
    pdf(paste(outFolder, "/PCA.", suffix, ".top", topN, ".groups.pdf", sep=""), width=12, height=6)
    layout(matrix(1:2, ncol=2))
    plotLoads(scaled_pca$x[,1], scaled_pca$x[,2], axisLbls[1], axisLbls[2], cols, TRUE)
    legend("bottomleft", legend=unique(group_label), fill=unique(cols), cex=0.75)
    plotLoads(scaled_pca$x[,1], scaled_pca$x[,2], axisLbls[1], axisLbls[2], cols, FALSE)
    legend("bottomleft", legend=unique(group_label), fill=unique(cols), cex=0.75)
    dev.off()
    #
    h <- 1.2+0.4*(length(unique(group_label))-1)
    #
    pdf(paste(outFolder, "/PCA.", suffix, ".top", topN, ".groups.boxplots.pdf", sep=""), width=3.5, height=h*nComp)
    layout(matrix(1:nComp, ncol=1))
    par(mar=c(3,5,5,1))
    for (pc in 1:nComp) bplot(scaled_pca, pc, axisLbls[pc], as.character(group_label), cols_2, range(scaled_pca$x[,pc]), test)
    dev.off()
    #
    return(scaled_pca)
}

degsCalc <- function(d, group, pair) {
    genes <- DGEList(d, group = group)
    # at least 3 samples with FPM >= 1
    genes <- genes[rowSums(1e+06 * genes$counts/expandAsMatrix(genes$samples$lib.size, dim(genes)) > 3) >= 1,]
    # estimate common dispersion
    genes <- estimateCommonDisp(genes)
    # tagwise dispersion (stick to default = 10)
    genes <- estimateTagwiseDisp(genes, prior.df = 10)
    # summary(genes$tagwise.dispersion)
    # TMM normalization
    genes <- calcNormFactors(genes, method="TMM")
    # testing
    genes.deg <- exactTest(genes, dispersion = "auto", pair = pair)
    #topTags(genes.deg, n=100, sort.by = "p.value")
    ps <- genes.deg$table[,"PValue"]
    # FDR
    bhs <- p.adjust(ps, method = "BH")
    # merge fc/p-val, FDR, counts
    out <- cbind(genes$counts, genes.deg$table, bhs)
    colnames(out)[ncol(out)] <- "FDR"
    return(out)
}

degsFilterSave <- function(degs, fc_t, fdr_t, i, suffix) {
    #
    #up-reg
    f <- degs$logFC >= fc_t & degs$FDR <= fdr_t
    degs_up <- sort(rownames(degs[f,]))
    outF <- paste(outFolder, "/", i, ".degs.", suffix, ".up.txt", sep="")
    write.table(degs_up, file=outF, sep="\t", row.names=F, col.names=F, quote=F)
    #down-reg
    f <- degs$logFC <= -fc_t & degs$FDR <= fdr_t
    degs_down <- sort(rownames(degs[f,]))
    outF <- paste(outFolder, "/", i, ".degs.", suffix, ".down.txt", sep="")
    write.table(degs_down, file=outF, sep="\t", row.names=F, col.names=F, quote=F)
    #
    return(list(up=degs_up, down=degs_down))
}

phyper_Ext <- function(set_fetal, set_dcm, N) {
    #
    q <- length(intersect(set_fetal, set_dcm))
    k <- length(set_dcm)
    m <- length(set_fetal)
    n <- N - m
    #
    dcm_frac <- q / k
    p <- phyper(q, m, n, k, lower.tail = FALSE, log.p = FALSE)
    #
    return(list(dcm_frac=dcm_frac, p=p))
}
